'use strict';

import {double} from '../lib/double';

it('works with `import`', () => {
  expect(double(5)).toBe(10);
});
