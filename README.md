# Template for Babel 7 with Jest

A simple template with basic setup for general purpose.

## Dependencies
This setup assumes you have **node** and **yarn** installed

## How-to to use
Start cloning this repo into your project folder
Then run:
~~~
yarn install --dev
~~~

## Running test
Simply use `yarn test` to run your tests

## Tips
You may want to run `git init` again to clean the repo to only contain your project files
and set the correct origin.
